﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using StripePayment.Models;

namespace StripePayment.Controllers
{
    public class PaymentController : ApiController
    {
        [HttpGet]
        public PaymentModel ChargePayment(string id)
        {
            try
            {
                PaymentModel paymentmodel = new PaymentModel();
                StripeConfiguration.SetApiKey("sk_test_LYEzMVFFdR6o4BeH778WzhY3");
                var options = new ChargeCreateOptions
                {
                    Amount = 500,
                    Currency = "usd",
                    Description = "Dummy payment",
                    SourceId = id,
                };
                var service = new ChargeService();
                Charge charge = service.Create(options);
                paymentmodel.status = charge.Status;
                return paymentmodel;

            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
